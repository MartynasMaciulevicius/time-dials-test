(ns time-dials-test.core-test
  (:require [clojure.test :refer :all]
            [time-dials-test.core :as a]))

(deftest angle-tests
  (testing "sample test"
    (is (= 0 (a/angle 0 12)))
    (is (= 30 (a/angle 1 12)))
    (is (= 60 (a/angle 2 12)))
    (is (= 90 (a/angle 3 12)))))

(deftest angle-hour-tests
  (testing "sample test"
    (is (= 0 (a/angle-hour 0)))
    (is (= 30 (a/angle-hour 1)))
    (is (= 60 (a/angle-hour 2)))
    (is (= 90 (a/angle-hour 3)))))

#_(deftest angle-min-tests
  (testing "sample test"
    (is (= 0 (a/angle-min 0)))
    (is (= 6 (a/angle-min 1)))
    (is (= 12 (a/angle-min 2)))
    (is (= 18 (a/angle-min 3)))))

(deftest angle-min-tests
  (testing "sample test"
    (is (= (int (+ 15 (/ 5 4))) (a/minute-dial-minutes 3)))
    (is (= (int (+ 30 (/ 5 2))) (a/minute-dial-minutes 6)))
    (is (= (int (+ 5 (/ 5 12))) (a/minute-dial-minutes 1)))))

(mod (+ (a/angle 1 12)
        (a/angle 1 60))
     60)

(defn calc [hours]
  (let [mins (Math/round (float (+ (/ 5 60) ;; 5 minutes
                                   ;; minute dial moves because hour dial moves 1/60*12 of 360
                                   (/ hours (* 60 5)))))
        hours (+ (a/angle hours 12) ;; hours
                 ;; move because of mins
                 (a/angle hours 60))]
    (int (/ mins 60))
    ))
(calc 1)
#_(def mins (Math/round (float (+ (a/angle 5 60) ;; 5 minutes
                                  (a/angle 1 (* 60 12))) ;; minute dial moves because hour dial moves 1/60*12 of 360
                               )))

#_(def hours (+ (a/angle 1 12) ;; hours
                (a/angle 1 60) ;; move because of mins
                ))

(deftest hour-tests
  (testing "sample test"
    (is (= "1:06"
           (a/method 1)))))
