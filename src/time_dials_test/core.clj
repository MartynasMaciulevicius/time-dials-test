(ns time-dials-test.core)

(defn angle [tick granularity]
  (* 360 (/ tick granularity)))

(defn angle-hour [tick]
  (angle tick 12))

(defn angle-min [tick]
  (angle tick 60))

(defn minute-dial-minutes [hour]
  (Math/round (float (+ (* 5 hour) (/ 5 (/ 12 hour))))))
#_(float (+ (* 5 1) (/ 5 (/ 12 1))))
#_(minute-dial-minutes 1)

(defn method [hour]
  "1:06"
  )

#_(method 5 1)
